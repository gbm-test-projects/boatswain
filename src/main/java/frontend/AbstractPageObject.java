package frontend;

import drivermanager.EnvironmentSetup;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class AbstractPageObject extends EnvironmentSetup{
    private WebDriverWait wait;
    private WebDriver driver;
    public AbstractPageObject(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        PageFactory.initElements(driver, this);
    }

    protected WebDriverWait wait(int waitTime) {
        setWait(new WebDriverWait(getDriver(), Duration.ofSeconds(waitTime)));
        return getWait();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWait() {
        return wait;
    }

    public void setWait(WebDriverWait wait) {
        this.wait = wait;
    }

    abstract public void waitForPageToLoad();
}
