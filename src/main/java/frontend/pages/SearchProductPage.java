package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SearchProductPage extends AbstractPageObject {
    private static final String SEARCH_RESULT_TITLE_CSS = ".title-phrasing-xl";

    @FindBy(css = SEARCH_RESULT_TITLE_CSS)
    WebElement searchResultTitle;

    public SearchProductPage(WebDriver driver) {
        super(driver);
    }

    public void isTheSearchResultCorrect(String keyword) {
        Assert.assertTrue(searchResultTitle.getText().contains(keyword),
                "The text was supposed to contain - '" + keyword + "' - but it did not.");
    }

    @Override
    public void waitForPageToLoad() {

    }
}
