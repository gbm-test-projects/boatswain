package frontend.pages;

import frontend.AbstractPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HeaderPage extends AbstractPageObject {
    private static final String SEARCH_BOX_ID = "searchboxTrigger";
    private static final String SEARCH_SUBMIT_BUTTON_CSS = ".searchbox-submit-button";
//    public WebElement searchBox = driver.findElement(By.id(SEARCH_BOX_ID));
//    public WebElement submitSearchButton = driver.findElement(By.cssSelector(SEARCH_SUBMIT_BUTTON_CSS));

    public HeaderPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = SEARCH_BOX_ID)
    WebElement searchBox;
    @FindBy(css = SEARCH_SUBMIT_BUTTON_CSS)
    WebElement submitSearchButton;

    public HeaderPage navigateToHeader() {
        getDriver().navigate().to(getUrl());
        return new HeaderPage(getDriver());
    }

    public HeaderPage justTypeInSearch(String keyword) {
        searchBox.sendKeys(keyword);
        return this;
    }

    public SearchProductPage typeInSearchAndSubmit(String keyword) {
        searchBox.clear();
        justTypeInSearch(keyword);
        submitSearchButton.click();
        return new SearchProductPage(getDriver());
    }

    @Override
    public void waitForPageToLoad() {
        getWait().until(ExpectedConditions.elementToBeClickable(submitSearchButton));
    }
}
