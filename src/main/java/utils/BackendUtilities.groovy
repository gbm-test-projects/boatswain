package utils

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

import java.time.LocalDate

class BackendUtilities {

    public MultiValueMap<String, String> turnPojoToMultiValueMap(Object object) {
        MultiValueMap valueMap = new LinkedMultiValueMap<String, Object>();
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, Object> fieldMap = objectMapper.convertValue(
                object, new TypeReference<Map<String, Object>>() {});
        valueMap.setAll(fieldMap);
        return valueMap;
    }

    public Document parseDocumentFromString(String stringToParse) {
        return Jsoup.parse(stringToParse);
    }

    def mapJsonToObject(String json, Class desiredClass) {
        ObjectMapper objectMapper = new ObjectMapper()
        return objectMapper.readValue(json, desiredClass)
    }

    def mapJsonToObjectList(String json, Class desiredClass) {
        ObjectMapper objectMapper = new ObjectMapper()
        return objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(
                List.class, desiredClass))
    }

    def mapObjectToJson(Object object) {
        ObjectMapper objectMapper = new ObjectMapper()

        return objectMapper.writeValueAsString(object)
    }

    String formatDatesForReports(String year, String month, int yearOffset = 0, int monthOffset = 0) {
        String formatedYear = (Integer.parseInt(year) + yearOffset).toString().split("(?<=\\G.{2})")[1]
        String formatedMonth = (Integer.parseInt(month) + monthOffset).toString()
        if (formatedMonth.length() == 1) {
            formatedMonth = "0" + formatedMonth
        }
        return formatedYear + formatedMonth
    }

    String formatOverheadDate(String year, String month, String day) {
        return year + "-0" + month + "-" + day
    }

    String formatYearMonth(String year = LocalDate.now().getYear().toString(),
                           String month = LocalDate.now().getMonthValue().toString()) {
        BackendUtilities beUtils = new BackendUtilities()
        return beUtils.formatDatesForReports(year, month)
    }
}