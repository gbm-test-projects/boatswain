package utils;

public class TestVariables {
    //Authentication
    public static final String USERNAME = "admin";
    public static final String PASSWORD = "admin";
    public static final String IPM_CREDENTIAL_ID = "";
    public static final String IPM_GRANT_TYPE = "password";
    public static final String IPM_CLIENT_ID = "automated_test";
    public static final String IPM_SCOPE = "openid";

    //Misc
    public static final String AUTOMATION_NAME_PREFIX = "Automated ";
    public static final String AUTOMATION_EDITED_NAME_PREFIX = "Automated Edited";
}