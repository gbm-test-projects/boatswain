package backend.payloaddtos.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AuthResponse {
    public String access_token;
    public int expires_in;
    public int refresh_expires_in;
    public String refresh_token;
    public String token_type;
    public String id_token;
    @JsonProperty("not-before-policy")
    public int notBeforePolicy;
    public String session_state;
    public String scope;

    public String getScope() {
        return scope;
    }

    public int getExpires_in() {
        return expires_in;
    }

    public int getNotBeforePolicy() {
        return notBeforePolicy;
    }

    public int getRefresh_expires_in() {
        return refresh_expires_in;
    }

    public String getAccess_token() {
        return access_token;
    }

    public String getId_token() {
        return id_token;
    }

    public String getRefresh_token() {
        return refresh_token;
    }

    public String getSession_state() {
        return session_state;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public void setExpires_in(int expires_in) {
        this.expires_in = expires_in;
    }

    public void setId_token(String id_token) {
        this.id_token = id_token;
    }

    public void setNotBeforePolicy(int notBeforePolicy) {
        this.notBeforePolicy = notBeforePolicy;
    }

    public void setRefresh_expires_in(int refresh_expires_in) {
        this.refresh_expires_in = refresh_expires_in;
    }

    public void setRefresh_token(String refresh_token) {
        this.refresh_token = refresh_token;
    }

    public void setSession_state(String session_state) {
        this.session_state = session_state;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }
}
