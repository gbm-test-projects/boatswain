package backend.payloaddtos.bodies;

public class AuthFormData {
    private String username;
    private String password;
    private String credentialId;
    private String grant_type;
    private String client_id;
    private String scope;
    private String client_secret;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCredentialId() {
        return credentialId;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public String getClient_id() {
        return client_id;
    }

    public String getScope() {
        return scope;
    }

    public String getClient_secret() {
        return client_secret;
    }

    public void setCredentialId(String credentialId) {
        this.credentialId = credentialId;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setGrant_type(String grant_type) {
        this.grant_type = grant_type;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setClient_secret(String client_secret) {
        this.client_secret = client_secret;
    }
}