package backend.specs


import backend.payloaddtos.bodies.AuthFormData
import backend.payloaddtos.responses.AuthResponse
import backend.RequestGenerator
import utils.BackendUtilities
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.util.MultiValueMap
import static utils.TestVariables.*

public class AuthSpec extends RequestGenerator {
    public AuthSpec() {
        setBaseURL(getUrl())
        setRequestUri("/route/for/endpoint")
        setBodyValues(convertFormDataToBody())
        headers = createHeaders()
        headers.set("origin", getUrl())
        headers.set("host", getSimpleUrl())
        debugReport()
    }
    BackendUtilities beUtils = new BackendUtilities()

    public AuthFormData generateFormData() {
        AuthFormData authFormData = new AuthFormData()
        authFormData.setUsername(USERNAME)
        authFormData.setPassword(PASSWORD)
        authFormData.setGrant_type("password")
        authFormData.setClient_id("automated_test")
        authFormData.setScope("openid")
        authFormData.setClient_secret("whateverSecret")

        return authFormData
    }

    @Override
    protected HttpHeaders createHeaders() {
        HttpHeaders headers = new HttpHeaders()
        headers.add(HttpHeaders.ACCEPT, "*/*");
        headers.add(HttpHeaders.ORIGIN, envSetup.getUrl());
        headers.add(HttpHeaders.ACCEPT_ENCODING, "gzip, deflate, br");
        headers.add(HttpHeaders.CONNECTION, "keep-alive");
        headers.add(HttpHeaders.HOST, getSimpleUrl());
        headers.add(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded");

        return headers;
    }

    public MultiValueMap<String, String> convertFormDataToBody() {
        return beUtils.turnPojoToMultiValueMap(generateFormData())
    }

    protected MultiValueMap<String, String> convertFormDataToQueryParams(AuthFormData authFormData = generateFormData()) {
        return beUtils.turnPojoToMultiValueMap(authFormData)
    }

    void setBearerToken(AuthResponse response) {
        this.bearerToken = "Bearer " + response.access_token
    }

    AuthResponse getAuthResponseFromResponseEntity(ResponseEntity<String> bearerTokenResponse) {
        return beUtils.mapJsonToObject(bearerTokenResponse.toString().substring(
                bearerTokenResponse.toString().indexOf(",") + 1, bearerTokenResponse.toString().indexOf(">")),
                AuthResponse.class) as AuthResponse
    }

    AuthResponse getAuthResponseFromMono(String responseMono) {
        return beUtils.mapJsonToObject(responseMono, AuthResponse.class) as AuthResponse
    }

    ResponseEntity<String> postAuth(MultiValueMap queryParams = this.queryParams,
                                    String baseURL = this.baseURL,
                                    requestUri = this.requestUri,
                                    MultiValueMap bodyValues = this.bodyValues,
                                    HttpHeaders neededHeaders = this.headers){

        return makePostRequest(queryParams, baseURL, requestUri, bodyValues, neededHeaders)
    }
    AuthResponse postKeycloakAuthForMono(MultiValueMap queryParams = this.queryParams,
                                         String baseURL = this.baseURL,
                                         requestUri = this.requestUri,
                                         MultiValueMap bodyValues = this.bodyValues,
                                         HttpHeaders neededHeaders = this.headers) {

        return getAuthResponseFromMono(makePostRequestToMono(
                queryParams, baseURL, requestUri, bodyValues, neededHeaders))
    }

    ResponseEntity<String> getAuth() {

        return makeGETRequestToMono()
    }
}
