package backend

import drivermanager.EnvironmentSetup
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

public abstract class RequestGenerator extends EnvironmentSetup{
    String baseURL
    WebClient client
    MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>()
    String requestUri
    EnvironmentSetup envSetup = new EnvironmentSetup()
    MultiValueMap queryParams = new LinkedMultiValueMap()
    HttpHeaders headers = new HttpHeaders()
    String bearerToken;
    String jsonBodyValues

    public WebClient createClient(String baseURL) {
        if (client == null) {
            setClient(WebClient.builder().baseUrl(baseURL).build());
        }
        return client;
    }

    protected abstract HttpHeaders createHeaders()

    protected ResponseEntity<String> makePostRequest(MultiValueMap queryParams = this.queryParams,
                                                     String baseURL = this.baseURL,
                                                     requestUri = this.requestUri,
                                                     MultiValueMap bodyValues = this.bodyValues,
                                                     HttpHeaders neededHeaders = this.headers) {
        return createClient(baseURL).post()
                .uri(uriBuilder ->
                        uriBuilder.path(requestUri)
                                .queryParams(queryParams)
                                .build())
                .headers(headers -> {
                    headers.addAll(neededHeaders)
                })
                .body(BodyInserters.fromFormData(bodyValues))
                .retrieve()
                .toEntity(String.class)
                .block()
    }

    protected String makePostRequestToMono(MultiValueMap queryParams = this.queryParams,
                                           String baseURL = this.baseURL,
                                           requestUri = this.requestUri,
                                           MultiValueMap bodyValues = this.bodyValues,
                                           HttpHeaders neededHeaders = this.headers) {
        return createClient(baseURL).post()
                .uri(uriBuilder ->
                        uriBuilder.path(requestUri)
                                .queryParams(queryParams)
                                .build())
                .headers(headers -> {
                    headers.addAll(neededHeaders)
                })
                .body(BodyInserters.fromFormData(bodyValues))
                .retrieve()
                .bodyToMono(String.class)
                .block()
    }

    protected String makePostRequestToMonoWithJsonBody(MultiValueMap queryParams = this.queryParams,
                                                       String baseURL = this.baseURL,
                                                       requestUri = this.requestUri,
                                                       String jsonBodyValues = this.jsonBodyValues,
                                                       HttpHeaders neededHeaders = this.headers) {
        return createClient(baseURL).post()
                .uri(uriBuilder ->
                        uriBuilder.path(requestUri)
                                .queryParams(queryParams)
                                .build())
                .headers(headers -> {
                    headers.addAll(neededHeaders)
                })
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonBodyValues)
                .retrieve()
                .bodyToMono(String.class)
                .block()
    }

    protected String makePutRequestToMonoWithJsonBody(MultiValueMap queryParams = this.queryParams,
                                                      String baseURL = this.baseURL,
                                                      requestUri = this.requestUri,
                                                      String jsonBodyValues = this.jsonBodyValues,
                                                      HttpHeaders neededHeaders = this.headers) {
        return createClient(baseURL).put()
                .uri(uriBuilder ->
                        uriBuilder.path(requestUri)
                                .queryParams(queryParams)
                                .build())
                .headers(headers -> {
                    headers.addAll(neededHeaders)
                })
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(jsonBodyValues)
                .retrieve()
                .bodyToMono(String.class)
                .block()
    }

    protected String makePutRequestToMono(MultiValueMap queryParams = this.queryParams,
                                          String baseURL = this.baseURL,
                                          requestUri = this.requestUri,
                                          MultiValueMap bodyValues = this.bodyValues,
                                          HttpHeaders neededHeaders = this.headers) {
        return createClient(baseURL).put()
                .uri(uriBuilder ->
                        uriBuilder.path(requestUri)
                                .queryParams(queryParams)
                                .build())
                .headers(headers -> {
                    headers.addAll(neededHeaders)
                })
                .body(BodyInserters.fromFormData(bodyValues))
                .retrieve()
                .bodyToMono(String.class)
                .block()
    }

    protected String makeGETRequestToMono(MultiValueMap queryParams = this.queryParams,
                                          String baseURL = this.baseURL,
                                          requestUri = this.requestUri,
                                          HttpHeaders neededHeaders = this.headers) {
        Mono<String> response = WebClient.create(baseURL)
                .get()
                .uri(uriBuilder ->  uriBuilder
                        .path(requestUri)
                        .queryParams(queryParams)
                        .build())
                .headers(headers -> {
                    headers.addAll(neededHeaders)
                })
                .retrieve()
                .bodyToMono(String.class)

        return response.block()
    }

    protected debugReport() {
        System.out.println("---------------------------------------------------------------------")
        System.out.println("Request made to: " + baseURL + requestUri)
        System.out.println("Headers: " + headers)
        System.out.println("Query Params: " + queryParams)
        System.out.println("Body: " + bodyValues)
        System.out.println("JsonBody: " + jsonBodyValues)
        System.out.println("---------------------------------------------------------------------")
    }

    public String getRequestUri() {
        return this.requestUri;
    }

    public MultiValueMap<String, String> getBodyValues() {
        return this.bodyValues;
    }

    public String getBaseURLl() {
        return this.baseURL;
    }

    public WebClient getWebClient() {
        return this.client;
    }

    public void setRequestUri(String requestUri) {
        this.requestUri = requestUri;
    }

    public void setBodyValues(MultiValueMap<String, String> bodyValues) {
        this.bodyValues = bodyValues;
    }

    public void setBaseURL(String baseURL) {
        this.baseURL = baseURL;
    }

    public void setClient(WebClient client) {
        this.client = client;
    }
}
