package emagtests;

import drivermanager.DriverInitializer;
import frontend.pages.HeaderPage;
import frontend.pages.SearchProductPage;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EmagSearchTests extends DriverInitializer {
    HeaderPage headerPage;
    SearchProductPage searchProductPage;

    @BeforeClass
    public void setup() {
        initializeDriver();
        headerPage = new HeaderPage(getDriver());
        searchProductPage = new SearchProductPage(getDriver());
    }

    @Test
    public void SearchForRandomProductTest() {
        headerPage.typeInSearchAndSubmit("camera web")
                .isTheSearchResultCorrect("camera web");
    }

    @Test
    public void SearchForRandomProductTest2() {
        headerPage.typeInSearchAndSubmit("camera web")
                .isTheSearchResultCorrect("camera weeb");
    }

    @AfterClass
    private void tearDown(){
        getDriver().close();
    }
}
